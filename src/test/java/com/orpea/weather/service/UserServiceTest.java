package com.orpea.weather.service;

import com.orpea.weather.dto.UserDTO;
import com.orpea.weather.exception.DuplicateException;
import com.orpea.weather.exception.NotFoundException;
import com.orpea.weather.model.Role;
import com.orpea.weather.model.User;
import com.orpea.weather.model.enums.RoleEnum;
import com.orpea.weather.repository.RoleRepository;
import com.orpea.weather.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepo;

    @Mock
    private RoleRepository roleRepo;

    @Mock
    private PasswordEncoder passwordEncoder;

    private static UserDTO userDTO;

    private Role role;
    private User user;

    @BeforeAll
    static void init() {
        userDTO = new UserDTO("john", "John Snow", "password", "Coimbra");
    }

    @BeforeEach
    void setUp() {
        this.userService = new UserService(userRepo, roleRepo, passwordEncoder);
        role = new Role(null, RoleEnum.ROLE_USER.name());
        user = new User(null, "john", "John Snow", "password", "Coimbra",
                new HashSet<>(Collections.singletonList(role)));
    }

    @Test
    void saveUser_success_with_roles() {
        UserService userServiceSpy = Mockito.spy(this.userService);
        Role user_role = new Role(null, RoleEnum.ROLE_USER.name());
        Mockito.doReturn(user_role).when(userServiceSpy).getRole(ArgumentMatchers.anyString());
        Mockito.when(userRepo.findByUsername(ArgumentMatchers.anyString())).thenReturn(Optional.empty());
        Mockito.when(userRepo.save(ArgumentMatchers.any(User.class))).thenReturn(user);
        User response = userServiceSpy.saveUser(userDTO);
        Assertions.assertEquals(user, response);
    }

    @Test
    void saveUser_duplicate() {
        Mockito.when(userRepo.findByUsername(ArgumentMatchers.anyString())).thenReturn(Optional.of(user));
        DuplicateException exception = Assertions.assertThrows(DuplicateException.class, () -> this.userService.saveUser(userDTO));
        Assertions.assertEquals("User 'john' already exists in the database", exception.getMessage());
    }

    @Test
    void saveRole_success() {
        Mockito.when(roleRepo.findByName(ArgumentMatchers.anyString())).thenReturn(Optional.empty());
        Mockito.when(roleRepo.save(ArgumentMatchers.any(Role.class))).thenReturn(role);
        Role response = this.userService.saveRole(RoleEnum.ROLE_USER.name());
        Assertions.assertEquals(role, response);
    }

    @Test
    void saveRole_duplicate() {
        Mockito.when(roleRepo.findByName(ArgumentMatchers.anyString())).thenReturn(Optional.of(role));
        DuplicateException exception = Assertions.assertThrows(DuplicateException.class, () -> this.userService.saveRole(RoleEnum.ROLE_USER.name()));
        Assertions.assertEquals("Role 'ROLE_USER' already exists in the database", exception.getMessage());
    }

    @Test
    void getUser_success() {
        Mockito.when(userRepo.findByUsername(ArgumentMatchers.anyString())).thenReturn(Optional.of(user));
        User response = this.userService.getUser("john");
        Assertions.assertEquals(user, response);
    }

    @Test
    void getUser_not_found() {
        Mockito.when(userRepo.findByUsername(ArgumentMatchers.anyString())).thenReturn(Optional.empty());
        NotFoundException exception = Assertions.assertThrows(NotFoundException.class, () -> this.userService.getUser("john"));
        Assertions.assertEquals("User 'john' not found in the database", exception.getMessage());
    }

    @Test
    void getUserById_success() {
        Mockito.when(userRepo.findById(ArgumentMatchers.anyLong())).thenReturn(Optional.of(user));
        User response = this.userService.getUserById(1L);
        Assertions.assertEquals(user, response);
    }

    @Test
    void getUserById_not_found() {
        Mockito.when(userRepo.findById(ArgumentMatchers.anyLong())).thenReturn(Optional.empty());
        NotFoundException exception = Assertions.assertThrows(NotFoundException.class, () -> this.userService.getUserById(1L));
        Assertions.assertEquals("User id '1' not found in the database", exception.getMessage());
    }

    @Test
    void getRole_success() {
        Mockito.when(roleRepo.findByName(ArgumentMatchers.anyString())).thenReturn(Optional.of(role));
        Role response = this.userService.getRole(RoleEnum.ROLE_USER.name());
        Assertions.assertEquals(role, response);
    }

    @Test
    void getRole_not_found() {
        Mockito.when(roleRepo.findByName(ArgumentMatchers.anyString())).thenReturn(Optional.empty());
        NotFoundException exception = Assertions.assertThrows(NotFoundException.class, () -> this.userService.getRole(RoleEnum.ROLE_ADMIN.name()));
        Assertions.assertEquals("Role 'ROLE_ADMIN' not found in the database", exception.getMessage());
    }

    @Test
    void getRoles() {
        List<Role> roles = new ArrayList<>();
        roles.add(new Role(null, RoleEnum.ROLE_USER.name()));
        roles.add(new Role(null, RoleEnum.ROLE_ADMIN.name()));
        Mockito.when(roleRepo.findAll()).thenReturn(roles);
        List<Role> response = this.userService.getRoles();
        Assertions.assertEquals(roles, response);
        Assertions.assertEquals(2, response.size());
    }

    @Test
    void getUsers() {
        List<User> users = new ArrayList<>();
        users.add(new User(null, "user1", "User1", "password", "Lisboa", new HashSet<>()));
        users.add(new User(null, "user2", "User2", "password", "Lisboa", new HashSet<>()));
        Mockito.when(userRepo.findAll()).thenReturn(users);
        List<User> response = this.userService.getUsers();
        Assertions.assertEquals(users, response);
        Assertions.assertEquals(2, response.size());
    }

    @Test
    void addRoleToUser() {
        UserService userServiceSpy = Mockito.spy(this.userService);
        Role admin_role = new Role(null, RoleEnum.ROLE_ADMIN.name());
        Mockito.doReturn(user).when(userServiceSpy).getUser(ArgumentMatchers.anyString());
        Mockito.doReturn(admin_role).when(userServiceSpy).getRole(ArgumentMatchers.anyString());
        Mockito.when(userRepo.save(ArgumentMatchers.any(User.class))).thenReturn(user);
        User response = userServiceSpy.addRoleToUser("john", RoleEnum.ROLE_ADMIN.name());
        Assertions.assertEquals(user, response);
        Assertions.assertEquals(2, response.getRoles().size());
    }
}