package com.orpea.weather;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class WeatherApplicationTests {

    //Test class added ONLY to cover main() invocation not covered by application tests.
    @Test
    void contextLoads() {
        WeatherApplication.main(new String[]{});
    }
}
