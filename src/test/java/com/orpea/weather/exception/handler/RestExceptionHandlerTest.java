package com.orpea.weather.exception.handler;

import com.orpea.weather.exception.BadRequestException;
import com.orpea.weather.exception.DuplicateException;
import com.orpea.weather.exception.NotFoundException;
import com.orpea.weather.exception.response.ApiError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;

import java.lang.reflect.Executable;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class RestExceptionHandlerTest {

    @InjectMocks
    private RestExceptionHandler handler;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private MethodParameter methodParameter;

    @Mock
    private Executable executable;

    @BeforeEach
    void setUp() {
        this.handler = new RestExceptionHandler();
    }

    @Test
    void handleNotFoundException() {
        NotFoundException exception = new NotFoundException("Not found");
        ResponseEntity<Object> response = this.handler.handleNotFoundException(exception, null);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        ApiError apiError = (ApiError) response.getBody();
        Assertions.assertNotNull(apiError);
        Assertions.assertEquals(exception.getMessage(), apiError.getMessage());
    }

    @Test
    void handleDuplicateException() {
        DuplicateException exception = new DuplicateException("Duplicate record");
        ResponseEntity<Object> response = this.handler.handleDuplicateException(exception, null);
        Assertions.assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
        ApiError apiError = (ApiError) response.getBody();
        Assertions.assertNotNull(apiError);
        Assertions.assertEquals(exception.getMessage(), apiError.getMessage());
    }

    @Test
    void handleBadRequestException() {
        BadRequestException exception = new BadRequestException("Bad request");
        ResponseEntity<Object> response = this.handler.handleBadRequestException(exception, null);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        ApiError apiError = (ApiError) response.getBody();
        Assertions.assertNotNull(apiError);
        Assertions.assertEquals(exception.getMessage(), apiError.getMessage());
    }

    @Test
    void handleAccessDeniedException() {
        AccessDeniedException exception = new AccessDeniedException("Access denied");
        ResponseEntity<Object> response = this.handler.handleAccessDeniedException(exception, null);
        Assertions.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        ApiError apiError = (ApiError) response.getBody();
        Assertions.assertNotNull(apiError);
        Assertions.assertEquals("Access denied", apiError.getMessage());
    }

    @Test
    void handleAuthenticationException() {
        AuthenticationException exception = new BadCredentialsException("Wrong password");
        ResponseEntity<Object> response = this.handler.handleAuthenticationException(exception, null);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        ApiError apiError = (ApiError) response.getBody();
        Assertions.assertNotNull(apiError);
        Assertions.assertEquals("Invalid username or password", apiError.getMessage());
    }

    @Test
    void handleGenericException() {
        Exception exception = new Exception("Generic error");
        ResponseEntity<Object> response = this.handler.handleGenericException(exception, null);
        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        ApiError apiError = (ApiError) response.getBody();
        Assertions.assertNotNull(apiError);
        Assertions.assertEquals("Oops! Something went wrong", apiError.getMessage());
    }

    @Test
    void handleMissingServletRequestParameter() {
        MissingServletRequestParameterException exception = new MissingServletRequestParameterException("user", "String");
        ResponseEntity<Object> response = this.handler.handleMissingServletRequestParameter(exception, null, null, null);
        Assertions.assertEquals(HttpStatus.NOT_ACCEPTABLE, response.getStatusCode());
        ApiError apiError = (ApiError) response.getBody();
        Assertions.assertNotNull(apiError);
        Assertions.assertEquals("Invalid parameter/s - user", apiError.getMessage());
    }

    @Test
    void handleMethodArgumentNotValid() {
        List<ObjectError> errors = new ArrayList<>();
        FieldError error1 = new FieldError("username", "username", "username cannot be empty");
        FieldError error2 = new FieldError("password", "password", "password cannot be empty");
        errors.add(error1);
        errors.add(error2);

        MethodArgumentNotValidException methodArgumentNotValidException = new MethodArgumentNotValidException(methodParameter, bindingResult);
        Mockito.when(methodParameter.getParameterIndex()).thenReturn(0);
        Mockito.when(methodParameter.getExecutable()).thenReturn(executable);
        Mockito.when(executable.toGenericString()).thenReturn("");
        Mockito.when(bindingResult.getAllErrors()).thenReturn(errors);
        ResponseEntity<Object> response = this.handler.handleMethodArgumentNotValid(methodArgumentNotValidException, null, null, null);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        ApiError apiError = (ApiError) response.getBody();
        Assertions.assertNotNull(apiError);
        Assertions.assertEquals("Invalid Fields: username - username cannot be empty, password - password cannot be empty", apiError.getMessage());
    }

    @Test
    void handleHttpMessageNotReadable() {
        HttpMessageNotReadableException exception = new HttpMessageNotReadableException("Not readable");
        ResponseEntity<Object> response = this.handler.handleHttpMessageNotReadable(exception, null, null, null);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        ApiError apiError = (ApiError) response.getBody();
        Assertions.assertNotNull(apiError);
        Assertions.assertEquals("Malformed JSON request", apiError.getMessage());
    }
}