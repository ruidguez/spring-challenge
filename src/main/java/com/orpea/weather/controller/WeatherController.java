package com.orpea.weather.controller;

import com.orpea.weather.dto.WeatherDTO;
import com.orpea.weather.exception.BadRequestException;
import com.orpea.weather.service.WeatherService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class WeatherController {

    private final WeatherService weatherService;

    @GetMapping(value = "/weather/{cityName}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get current weather by city name")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Invalid Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 406, message = "Not Accepted"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
    })
    public ResponseEntity<WeatherDTO> getCurrentWeatherByCityName(
            @PathVariable("cityName") String cityName) {
        if (cityName == null || cityName.isBlank()) {
            throw new BadRequestException("City name is not valid.");
        }
        WeatherDTO weather = this.weatherService.getWeatherDetailsByCityName(cityName);
        return ResponseEntity.ok(weather);
    }

    @GetMapping(value = "/weather/health", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Check the health of weather service")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
    })
    public Health checkHealth() {
        String city = "Lisbon";
        WeatherDTO weather = this.weatherService.getWeatherDetailsByCityName(city);
        if (weather != null) {
            return Health.up().build();
        } else {
            return Health.outOfService().build();
        }
    }
}
