package com.orpea.weather;

import com.orpea.weather.dto.UserDTO;
import com.orpea.weather.model.enums.RoleEnum;
import com.orpea.weather.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class WeatherApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeatherApplication.class, args);
    }

    @Bean
    CommandLineRunner run(UserService userService) {
        return args -> {
            userService.saveRole(RoleEnum.ROLE_USER.name());
            userService.saveRole(RoleEnum.ROLE_ADMIN.name());

            userService.saveUser(new UserDTO("user", "User", "123456", "Porto"));
            userService.saveUser(new UserDTO("admin", "Admin", "123456", "Lisboa"));

            userService.addRoleToUser("admin", RoleEnum.ROLE_ADMIN.name());
        };
    }
}
