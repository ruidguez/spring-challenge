package com.orpea.weather.service;

import com.orpea.weather.dto.*;
import com.orpea.weather.exception.NotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

@ExtendWith(MockitoExtension.class)
class WeatherServiceTest {

    @InjectMocks
    private WeatherService weatherService;

    @Mock
    private RestTemplate restTemplate;

    @BeforeEach
    void setUp() {
        this.weatherService = new WeatherService(restTemplate);
    }

    @Test
    void getWeatherDetailsByCityName_success() {
        String body = "{\"coord\":{\"lon\":-9.1333,\"lat\":38.7167},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"base\":\"stations\",\"main\":{\"temp\":12.71,\"feels_like\":11.73,\"temp_min\":10.12,\"temp_max\":13.38,\"pressure\":1020,\"humidity\":65},\"visibility\":10000,\"wind\":{\"speed\":3.6,\"deg\":310},\"clouds\":{\"all\":0},\"dt\":1636320148,\"sys\":{\"type\":1,\"id\":6901,\"country\":\"PT\",\"sunrise\":1636269004,\"sunset\":1636306225},\"timezone\":0,\"id\":2267057,\"name\":\"Lisbon\",\"cod\":200}";
        Mockito.when(restTemplate.getForEntity(ArgumentMatchers.any(URI.class), ArgumentMatchers.any())).thenReturn(ResponseEntity.ok().body(body));
        WeatherDTO response = this.weatherService.getWeatherDetailsByCityName("Leiria");
        Assertions.assertEquals("Lisbon", response.getCity());
        Assertions.assertEquals("PT", response.getCountryCode());
        Assertions.assertNotNull(response.getCoordinates());
        Assertions.assertNotNull(response.getTemperature());
        Assertions.assertNotNull(response.getWind());
    }

    @Test
    void getWeatherDetailsByCityName_not_found() {
        Mockito.when(restTemplate.getForEntity(ArgumentMatchers.any(URI.class), ArgumentMatchers.any()))
                .thenThrow(new NotFoundException("City not found"));
        Assertions.assertThrows(NotFoundException.class, () -> this.weatherService.getWeatherDetailsByCityName("unknown"));
    }

    @Test
    void getWeatherDetailsByCityName_not_expected() {
        Mockito.when(restTemplate.getForEntity(ArgumentMatchers.any(URI.class), ArgumentMatchers.any())).thenReturn(ResponseEntity.badRequest().build());
        Assertions.assertThrows(Exception.class, () -> this.weatherService.getWeatherDetailsByCityName("Leiria"));
    }

}