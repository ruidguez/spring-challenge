package com.orpea.weather.model.enums;

public enum RoleEnum {
    ROLE_ADMIN,
    ROLE_USER
}
