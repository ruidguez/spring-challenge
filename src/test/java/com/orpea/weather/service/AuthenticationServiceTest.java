package com.orpea.weather.service;

import com.orpea.weather.model.Role;
import com.orpea.weather.model.User;
import com.orpea.weather.model.enums.RoleEnum;
import com.orpea.weather.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class AuthenticationServiceTest {

    @InjectMocks
    private AuthenticationService authenticationService;

    @Mock
    private UserRepository userRepo;

    private static User user;
    private static Role role;

    @BeforeAll
    static void init() {
        role = new Role(null, RoleEnum.ROLE_USER.name());
        user = new User(null, "john", "John Snow", "password", "Coimbra",
                new HashSet<>(Collections.singletonList(role)));
    }

    @BeforeEach
    void setUp() {
        this.authenticationService = new AuthenticationService(userRepo);
    }

    @Test
    void loadUserByUsername_success() {
        Mockito.when(userRepo.findByUsername(ArgumentMatchers.anyString())).thenReturn(Optional.of(user));
        UserDetails response = this.authenticationService.loadUserByUsername("john");
        Assertions.assertEquals(user.getUsername(), response.getUsername());
        Assertions.assertEquals(user.getPassword(), response.getPassword());
        Assertions.assertEquals(user.getAuthorities(), response.getAuthorities());
    }

    @Test
    void loadUserByUsername_not_found() {
        Mockito.when(userRepo.findByUsername(ArgumentMatchers.anyString())).thenReturn(Optional.empty());
        Assertions.assertThrows(UsernameNotFoundException.class,
                () -> this.authenticationService.loadUserByUsername("john"));
    }
}