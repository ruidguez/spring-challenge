package com.orpea.weather.controller;

import com.orpea.weather.dto.UserDTO;
import com.orpea.weather.exception.BadRequestException;
import com.orpea.weather.model.Role;
import com.orpea.weather.model.User;
import com.orpea.weather.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class UserController {

    private final UserService userService;

    @GetMapping(
            value = "/users",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get all users from database")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok().body(this.userService.getUsers());
    }

    @GetMapping(
            value = "/user",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get specific user by username")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Invalid Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 406, message = "Not Accepted"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ResponseEntity<User> getUser(@RequestParam("username") String username) {
        if (username == null || username.isBlank()) {
            throw new BadRequestException("Username is not valid.");
        }
        return ResponseEntity.ok().body(this.userService.getUser(username));
    }

    @PostMapping(
            value = "/user",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create new user in the database")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Success"),
            @ApiResponse(code = 400, message = "Invalid Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 406, message = "Not Accepted"),
            @ApiResponse(code = 409, message = "Duplicate Record"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ResponseEntity<User> saveUser(@Valid @RequestBody UserDTO userDTO) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/user").toUriString());
        return ResponseEntity.created(uri).body(this.userService.saveUser(userDTO));
    }

    @GetMapping(
            value = "/roles",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get all roles from the database")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ResponseEntity<List<Role>> getRoles() {
        return ResponseEntity.ok().body(this.userService.getRoles());
    }

    @PostMapping(
            value = "/role",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create new role in the database")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Success"),
            @ApiResponse(code = 400, message = "Invalid Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 406, message = "Not Accepted"),
            @ApiResponse(code = 409, message = "Duplicate Record"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Role> saveRole(@RequestParam("roleName") String roleName) {
        if (roleName == null || roleName.isBlank()) {
            throw new BadRequestException("Role name is not valid.");
        }
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/role").toUriString());
        return ResponseEntity.created(uri).body(this.userService.saveRole(roleName));
    }

    @PostMapping(
            value = "/role/addtouser",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Add new role for a specific user")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Invalid Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 406, message = "Not Accepted"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<User> addRoleToUser(
            @RequestParam("username") String username,
            @RequestParam("roleName") String roleName) {
        if (username == null || username.isBlank() || roleName == null || roleName.isBlank()) {
            throw new BadRequestException("At least one of the parameters is not valid.");
        }
        return ResponseEntity.ok().body(this.userService.addRoleToUser(username, roleName));
    }
}