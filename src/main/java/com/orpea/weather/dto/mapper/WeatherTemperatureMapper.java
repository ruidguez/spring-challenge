package com.orpea.weather.dto.mapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.orpea.weather.dto.WeatherTemperatureDTO;

public class WeatherTemperatureMapper {

    public static WeatherTemperatureDTO toWeatherTemperatureDTO(JsonNode root) {

        return WeatherTemperatureDTO.builder()
                .temperature(root.path("main").path("temp").asDouble())
                .temperature_min(root.path("main").path("temp_min").asDouble())
                .temperature_max(root.path("main").path("temp_max").asDouble())
                .temperature_sensation(root.path("main").path("feels_like").asDouble())
                .build();
    }
}
