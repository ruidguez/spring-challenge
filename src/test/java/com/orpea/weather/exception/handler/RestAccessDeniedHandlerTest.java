package com.orpea.weather.exception.handler;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ExtendWith(MockitoExtension.class)
class RestAccessDeniedHandlerTest {

    @InjectMocks
    private RestAccessDeniedHandler handler;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private AccessDeniedException accessDeniedException;

    @Mock
    private ServletOutputStream servletOutputStream;

    @Test
    void handle() throws IOException, ServletException {

        Mockito.when(response.getOutputStream()).thenReturn(servletOutputStream);
        this.handler.handle(request, response, accessDeniedException);

        Mockito.verify(response, Mockito.times(1)).setContentType(MediaType.APPLICATION_JSON_VALUE);
        Mockito.verify(response, Mockito.times(1)).setCharacterEncoding("UTF-8");
        Mockito.verify(response, Mockito.times(1)).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
}