package com.orpea.weather.service;

import com.orpea.weather.dto.UserDTO;
import com.orpea.weather.exception.DuplicateException;
import com.orpea.weather.exception.NotFoundException;
import com.orpea.weather.model.Role;
import com.orpea.weather.model.User;
import com.orpea.weather.model.enums.RoleEnum;
import com.orpea.weather.repository.RoleRepository;
import com.orpea.weather.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Data
@Transactional
@Slf4j
@Service
public class UserService {

    private final UserRepository userRepo;
    private final RoleRepository roleRepo;
    private final PasswordEncoder passwordEncoder;

    public User saveUser(UserDTO userDTO) {
        log.info("Saving new user {} to the database", userDTO.getUsername());
        Optional<User> userOpt = this.userRepo.findByUsername(userDTO.getUsername());
        if (userOpt.isPresent()) {
            log.info("User {} already exists in the database", userDTO.getUsername());
            throw new DuplicateException("User '" + userDTO.getUsername() + "' already exists in the database");
        }
        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setName(userDTO.getName());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setCity(userDTO.getCity());
        user.setRoles(new HashSet<>(Collections.singletonList(this.getRole(RoleEnum.ROLE_USER.name()))));
        return this.userRepo.save(user);
    }

    public Role saveRole(String roleName) {
        log.info("Saving new role {} to the database", roleName);
        Optional<Role> roleOpt = this.roleRepo.findByName(roleName);
        if (roleOpt.isPresent()) {
            log.info("Role {} already exists in the database", roleName);
            throw new DuplicateException("Role '" + roleName + "' already exists in the database");
        }
        Role role = new Role();
        role.setName(roleName);
        return this.roleRepo.save(role);
    }

    public User addRoleToUser(String username, String roleName) {
        log.info("Adding role {} to user {}", roleName, username);
        User user = this.getUser(username);
        Role role = this.getRole(roleName);
        user.getRoles().add(role);
        return this.userRepo.save(user);
    }

    public User getUser(String username) {
        log.info("Getting user {}", username);
        Optional<User> userOpt = this.userRepo.findByUsername(username);
        if (userOpt.isEmpty()) {
            log.error("User {} not found in the database", username);
            throw new NotFoundException("User '" + username + "' not found in the database");
        }
        log.info("User found in the database: {}", username);
        return userOpt.get();
    }

    public User getUserById(Long id) {
        log.info("Getting user id {}", id);
        Optional<User> userOpt = this.userRepo.findById(id);
        if (userOpt.isEmpty()) {
            log.error("User id {} not found in the database", id);
            throw new NotFoundException("User id '" + id + "' not found in the database");
        }
        log.info("User id found in the database: {}", id);
        return userOpt.get();
    }

    public Role getRole(String roleName) {
        log.info("Getting role {}", roleName);
        Optional<Role> userOpt = this.roleRepo.findByName(roleName);
        if (userOpt.isEmpty()) {
            log.error("Role {} not found in the database", roleName);
            throw new NotFoundException("Role '" + roleName + "' not found in the database");
        }
        log.info("Role found in the database: {}", roleName);
        return userOpt.get();
    }

    public List<Role> getRoles() {
        log.info("Getting all roles");
        return this.roleRepo.findAll();
    }

    public List<User> getUsers() {
        log.info("Getting all users");
        return this.userRepo.findAll();
    }
}
