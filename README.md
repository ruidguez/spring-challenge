# Java Spring Basic Challenge

* In this exercise several endpoints were created, most of them to manage users and their permissions in order to be able to show some spring security features
  and also an endpoint to get the weather conditions of a certain city as requested.

* The database used was _[H2][H2Link]_ and the information is being persisted only in memory. Check the endpoint `/h2-console` for more information. (Check configurations in _application.properties_)

* To check if the service is running, a _[Spring Boot Actuator][ActuatorLink]_ has also been included to monitor the application (like `/actuator/health`).

* Another dependency included in the project was Lombok to automatically generate some stuff and also to make code cleaner.

* If you want to check all available endpoints and more documentation, you can use _[Swagger][SwaggerLink]_ after starting the application.

### How can the client connect?

To make it easier to test, whenever the application starts there are two users by default:

* **User 1**:
    * Username: user
    * Name: User
    * Password: 123456
    * City: Porto
    * Roles: User


* **User 2**:
    * Username: admin
    * Name: Admin
    * Password: 123456
    * City: Lisboa
    * Roles: User, Admin

### Steps
1. Create a user (`POST /api/v1/user`) or use one that has already been created by default.
    
    1. Request example:
    
        ```json
        {
            "username": "rui",
            "password": "123456",
            "name": "Rui Lopes",
            "city": "Leiria"
        }
        ```

2. Get authentication token (`POST /api/v1/login`)
    
    1. Request example:
       
        ```json   
        {
           "username": "rui",
           "password": "123456"
        }
        ```
       
    2. Response example:
       
         ```json         
         {
            "type": "Bearer",
            "token": "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJXZWF0aGVyIiwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInN1YiI6ImpvaG4iLCJpYXQiOjE2MzYzMDM5NTYsImV4cCI6MTYzNjM5MDM1Nn0.QeMotio1l2S99W2MIbd4HwLdMISgv_Et6aaSTZnKGqw"
         }
         ```
       
3. Use the token returned in step 2 to access the other endpoints.

### Notes
* There are three endpoints you need to have the _ADMIN_ role to be able to access:
    * Get all users (`GET /api/v1/users`)
    * Create new role (`POST /api/v1/role`)
    * Add role to a user (`POST /api/v1/role/addroletouser`)

  For these cases, authentication is required by a user with the _ADMIN_ role.

---

Feel free to contact me if you have any question.
I know I could have implemented more features, but I believe it is enough for what was requested.

Thank you!

[ActuatorLink]: http://localhost:8080/actuator/health
[H2Link]: http://localhost:8080/h2-console
[SwaggerLink]: http://localhost:8080/swagger-ui/