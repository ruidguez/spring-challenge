package com.orpea.weather.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.orpea.weather.dto.UserDTO;
import com.orpea.weather.model.Role;
import com.orpea.weather.model.User;
import com.orpea.weather.model.enums.RoleEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
class UserControllerTest extends BaseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private Role role;
    private User user;

    @BeforeEach
    void setUp() {
        role = new Role(null, RoleEnum.ROLE_USER.name());
        user = new User(null, "john", "John Snow", "password", "Coimbra",
                new HashSet<>(Collections.singletonList(role)));
    }

    @Test
    void getUsers_success() throws Exception {
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.getUsers()).thenReturn(users);

        mockMvc.perform(get("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].username", is(user.getUsername())));
    }

    @Test
    void getUser_success() throws Exception {
        Mockito.when(userService.getUser(ArgumentMatchers.anyString())).thenReturn(user);

        mockMvc.perform(get("/api/v1/user")
                .param("username", user.getUsername())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", is(user.getUsername())));
    }

    @Test
    void getUser_bad_request() throws Exception {
        mockMvc.perform(get("/api/v1/user")
                .param("username", "")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveUser() throws Exception {
        Mockito.when(userService.saveUser(ArgumentMatchers.any(UserDTO.class))).thenReturn(user);

        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
        String content = ow.writeValueAsString(user);

        mockMvc.perform(post("/api/v1/user")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.username", is(user.getUsername())));
    }

    @Test
    void getRoles() throws Exception {
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        Mockito.when(userService.getRoles()).thenReturn(roles);

        mockMvc.perform(get("/api/v1/roles")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(RoleEnum.ROLE_USER.name())));
    }

    @Test
    void saveRole_success() throws Exception {
        Mockito.when(userService.saveRole(ArgumentMatchers.anyString())).thenReturn(role);

        mockMvc.perform(post("/api/v1/role")
                .param("roleName", RoleEnum.ROLE_USER.name())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(role.getName())));
    }

    @Test
    void saveRole_bad_request() throws Exception {
        mockMvc.perform(post("/api/v1/role")
                .param("roleName", "")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void addRoleToUser_success() throws Exception {
        Mockito.when(userService.addRoleToUser(ArgumentMatchers.anyString(), ArgumentMatchers.anyString()))
                .thenReturn(user);

        mockMvc.perform(post("/api/v1/role/addtouser")
                .param("username", user.getUsername())
                .param("roleName", role.getName())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", is(user.getUsername())));
    }

    @Test
    void addRoleToUser_bad_request() throws Exception {
        mockMvc.perform(post("/api/v1/role/addtouser")
                .param("username", "")
                .param("roleName", role.getName())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest());
    }

}