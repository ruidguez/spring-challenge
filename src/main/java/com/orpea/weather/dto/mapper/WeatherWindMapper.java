package com.orpea.weather.dto.mapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.orpea.weather.dto.WeatherWindDTO;

public class WeatherWindMapper {

    public static WeatherWindDTO toWeatherWindDTO(JsonNode root) {

        return WeatherWindDTO.builder()
                .speed(root.path("wind").path("speed").asDouble())
                .degreeDirection(root.path("wind").path("deg").asInt())
                .build();
    }
}
