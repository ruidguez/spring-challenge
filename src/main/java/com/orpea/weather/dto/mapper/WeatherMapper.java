package com.orpea.weather.dto.mapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.orpea.weather.dto.WeatherDTO;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

public class WeatherMapper {

    public static WeatherDTO toWeatherDTO(JsonNode root) {

        return WeatherDTO.builder()
                .city(root.path("name").asText())
                .countryCode(root.path("sys").path("country").asText())
                .coordinates(WeatherCoordinatesMapper.toWeatherCoordinatesDTO(root))
                .temperature(WeatherTemperatureMapper.toWeatherTemperatureDTO(root))
                .wind(WeatherWindMapper.toWeatherWindDTO(root))
                .visibility(root.path("visibility").asInt())
                .cloudiness(root.path("clouds").path("all").asInt())
                .pressure(root.path("main").path("pressure").asInt())
                .humidity(root.path("main").path("humidity").asInt())
                .sunrise(convertToLocalDateTime(root.path("sys").path("sunrise").asLong()))
                .sunset(convertToLocalDateTime(root.path("sys").path("sunset").asLong()))
                .now(convertToLocalDateTime(root.path("dt").asLong())).build();
    }

    private static LocalDateTime convertToLocalDateTime(long timestamp) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp), TimeZone.getDefault().toZoneId());
    }

}
