package com.orpea.weather.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orpea.weather.dto.WeatherDTO;
import com.orpea.weather.dto.mapper.WeatherMapper;
import com.orpea.weather.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.net.URI;

@Service
@RequiredArgsConstructor
@Slf4j
public class WeatherService {

    @Value("${weather.api.key}")
    private String apiKey;

    private final RestTemplate restTemplate;

    public WeatherDTO getWeatherDetailsByCityName(String cityName) {
        log.info("Getting weather details for city {} ", cityName);
        String url = "http://api.openweathermap.org/data/2.5/weather?q={city}&appid={key}&units=metric";
        URI uri = new UriTemplate(url).expand(cityName, apiKey);
        try {
            ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
            if (HttpStatus.OK.equals(response.getStatusCode())) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode root = mapper.readTree(response.getBody());
                return WeatherMapper.toWeatherDTO(root);
            } else {
                log.error("Weather API response not expected: {}", response.getStatusCode());
                throw new Exception("Weather API response not expected");
            }
        } catch (Exception ex) {
            log.info("City '{}' not found", cityName);
            throw new NotFoundException("City '" + cityName + "' not found");
        }
    }
}
