package com.orpea.weather.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WeatherTemperatureDTO {

    private double temperature;

    private double temperature_min;

    private double temperature_max;

    private double temperature_sensation;

}
