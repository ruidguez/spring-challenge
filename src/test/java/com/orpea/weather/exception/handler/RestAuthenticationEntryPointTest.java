package com.orpea.weather.exception.handler;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ExtendWith(MockitoExtension.class)
class RestAuthenticationEntryPointTest {

    @InjectMocks
    private RestAuthenticationEntryPoint handler;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private AuthenticationException authEx;

    @Mock
    private ServletOutputStream servletOutputStream;

    @Test
    void commence() throws IOException {
        Mockito.when(response.getOutputStream()).thenReturn(servletOutputStream);
        this.handler.commence(request, response, authEx);
        Mockito.verify(response, Mockito.times(1)).setContentType(MediaType.APPLICATION_JSON_VALUE);
        Mockito.verify(response, Mockito.times(1)).setCharacterEncoding("UTF-8");
        Mockito.verify(response, Mockito.times(1)).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
}