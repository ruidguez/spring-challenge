package com.orpea.weather.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WeatherWindDTO {

    private double speed;

    private int degreeDirection;

}
