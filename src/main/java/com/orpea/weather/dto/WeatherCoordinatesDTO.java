package com.orpea.weather.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WeatherCoordinatesDTO {

    private double longitude;

    private double latitude;

}
