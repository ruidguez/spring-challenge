package com.orpea.weather.dto.mapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.orpea.weather.dto.WeatherCoordinatesDTO;

public class WeatherCoordinatesMapper {

    public static WeatherCoordinatesDTO toWeatherCoordinatesDTO(JsonNode root) {

        return WeatherCoordinatesDTO.builder()
                .latitude(root.path("coord").path("lat").asDouble())
                .longitude(root.path("coord").path("lon").asDouble())
                .build();
    }
}
