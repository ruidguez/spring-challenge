package com.orpea.weather.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orpea.weather.dto.WeatherDTO;
import com.orpea.weather.service.WeatherService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(WeatherController.class)
class WeatherControllerTest extends BaseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private WeatherService weatherService;

    @Test
    void getCurrentWeatherByCityName_success() throws Exception {
        WeatherDTO weatherDTO = WeatherDTO.builder().city("Lisbon").build();

        Mockito.when(weatherService.getWeatherDetailsByCityName(ArgumentMatchers.anyString())).thenReturn(weatherDTO);

        MvcResult result = mockMvc.perform(get("/api/v1/weather/{cityName}", "Leiria")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        WeatherDTO response = objectMapper.readValue(content, WeatherDTO.class);
        Assertions.assertEquals(weatherDTO.getCity(), response.getCity());
    }

    @Test
    void getCurrentWeatherByCityName_invalid_city() throws Exception {
        mockMvc.perform(get("/api/v1/weather/{cityName}", " ")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    void checkHealth_success() throws Exception {
        WeatherDTO weatherDTO = WeatherDTO.builder().city("Lisbon").build();
        Mockito.when(weatherService.getWeatherDetailsByCityName(ArgumentMatchers.anyString())).thenReturn(weatherDTO);

        mockMvc.perform(get("/api/v1/weather/health")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isOk());
    }

    @Test
    void checkHealth_out_of_service() throws Exception {
        Mockito.when(weatherService.getWeatherDetailsByCityName(ArgumentMatchers.anyString())).thenReturn(null);

        mockMvc.perform(get("/api/v1/weather/health")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isOk());
    }
}