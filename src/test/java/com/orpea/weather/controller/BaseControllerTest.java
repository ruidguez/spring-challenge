package com.orpea.weather.controller;

import com.orpea.weather.exception.handler.RestAccessDeniedHandler;
import com.orpea.weather.exception.handler.RestAuthenticationEntryPoint;
import com.orpea.weather.service.AuthenticationService;
import com.orpea.weather.service.TokenService;
import com.orpea.weather.service.UserService;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;

@WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
public abstract class BaseControllerTest {

    @MockBean
    protected AuthenticationService authenticationService;

    @MockBean
    protected TokenService tokenService;

    @MockBean
    protected UserService userService;

    @MockBean
    protected BCryptPasswordEncoder bCryptPasswordEncoder;

    @MockBean
    protected RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @MockBean
    protected RestAccessDeniedHandler restAccessDeniedHandler;

}
