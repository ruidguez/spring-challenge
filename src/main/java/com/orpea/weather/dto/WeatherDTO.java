package com.orpea.weather.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class WeatherDTO {

    private String city;

    private String countryCode;

    private WeatherCoordinatesDTO coordinates;

    private WeatherTemperatureDTO temperature;

    private WeatherWindDTO wind;

    private int visibility;

    private double cloudiness;

    private int pressure;

    private int humidity;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime sunrise;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime sunset;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime now;

}
