package com.orpea.weather.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, value = HttpStatus.CONFLICT)
public class DuplicateException extends RuntimeException {

    private static final long serialVersionUID = 8073285452120372031L;

    public DuplicateException(String message) {
        super(message);
    }

}
