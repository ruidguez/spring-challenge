package com.orpea.weather.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.time.LocalDateTime;

@ExtendWith(MockitoExtension.class)
class LocalDateTimeDeserializerTest {

    @InjectMocks
    private LocalDateTimeDeserializer localDateTimeDeserializer;

    @Mock
    private JsonParser jsonParser;

    @Mock
    private DeserializationContext deserializationContext;

    @Mock
    private ObjectCodec objectCodec;

    @Mock
    private JsonNode jsonNode;

    @Test
    void deserialize() throws IOException {
        Mockito.when(jsonParser.getCodec()).thenReturn(objectCodec);
        Mockito.when(objectCodec.readTree(ArgumentMatchers.any(JsonParser.class))).thenReturn(jsonNode);
        Mockito.when(jsonNode.longValue()).thenReturn(1636199936L);
        LocalDateTime localDateTime = this.localDateTimeDeserializer.deserialize(jsonParser, deserializationContext);
        Assertions.assertEquals(2021, localDateTime.getYear());
        Assertions.assertEquals(11, localDateTime.getMonthValue());
        Assertions.assertEquals(6, localDateTime.getDayOfMonth());
        Assertions.assertEquals(11, localDateTime.getHour());
        Assertions.assertEquals(58, localDateTime.getMinute());
        Assertions.assertEquals(56, localDateTime.getSecond());
    }
}