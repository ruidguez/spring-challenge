package com.orpea.weather.exception.handler;

import com.orpea.weather.exception.BadRequestException;
import com.orpea.weather.exception.DuplicateException;
import com.orpea.weather.exception.NotFoundException;
import com.orpea.weather.exception.response.ApiError;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Iterator;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({NotFoundException.class})
    @ResponseBody
    public ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) {
        logger.error("Exception:- " + ex.getMessage(), ex);
        return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler({DuplicateException.class})
    @ResponseBody
    public ResponseEntity<Object> handleDuplicateException(DuplicateException ex, WebRequest request) {
        logger.error("Exception:- " + ex.getMessage(), ex);
        return buildResponseEntity(new ApiError(HttpStatus.CONFLICT, ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({BadRequestException.class})
    @ResponseBody
    public ResponseEntity<Object> handleBadRequestException(BadRequestException ex, WebRequest request) {
        logger.error("Exception:- " + ex.getMessage(), ex);
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler({AccessDeniedException.class})
    @ResponseBody
    public final ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
        logger.error("Exception:- " + ex.getMessage(), ex);
        return buildResponseEntity(new ApiError(HttpStatus.FORBIDDEN, ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({AuthenticationException.class})
    @ResponseBody
    public ResponseEntity<Object> handleAuthenticationException(AuthenticationException ex, WebRequest request) {
        logger.error("Exception:- " + ex.getMessage(), ex);
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, "Invalid username or password"));
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({Exception.class})
    @ResponseBody
    public ResponseEntity<Object> handleGenericException(Exception ex, WebRequest request) {
        logger.error("Exception:- " + ex.getMessage(), ex);
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Oops! Something went wrong"));
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        logger.error("Exception:- " + ex.getMessage(), ex);
        String message = "Invalid parameter/s - " + ex.getParameterName();
        return buildResponseEntity(new ApiError(HttpStatus.NOT_ACCEPTABLE, message, ex));
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        logger.error("Exception:- " + ex.getMessage(), ex);
        Iterator<ObjectError> errors = ex.getBindingResult().getAllErrors().iterator();
        StringBuilder message = new StringBuilder("Invalid Fields: ");
        String separator = "";
        StringBuilder debugMessage = new StringBuilder();

        while (errors.hasNext()) {
            ObjectError error = errors.next();
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();

            if (!message.toString().contains(fieldName)) {
                message.append(separator)
                        .append(fieldName)
                        .append(" - ")
                        .append(errorMessage);
                separator = ", ";
            }
            debugMessage.append(errorMessage).append(".");
        }
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, message.toString(), ex, debugMessage.toString()));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            HttpMessageNotReadableException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        logger.error("Exception:- " + ex.getMessage(), ex);
        String error = "Malformed JSON request";
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, HttpStatus.valueOf(apiError.getStatusCode()));
    }
}
